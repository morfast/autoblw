#!/usr/bin/python

import re
import sys
import os
import time
import getopt

class BLWblock:
    def __init__(self):
        self._coord_ = []
        self._charge_ = 0
        self._multi_ = 1
        self._name_ = []
        self._mtb_ = {}
        self._btm_ = {}
        self._natom_ = 0
        self._nb_ = 0
        self._nel_ = 0
        self._bas_ = [0,0]
    def add_coord(self, coord):
        self._coord_.append(coord)

def warn(msg):
    color_print("Warning: %s" % msg, "yellow", 1)

def error(msg):
    color_print("Error: %s" % msg, "red", 1)
    sys.exit(1)

def write_log(msg, file=sys.stdout):
    file.write("%s\n" % msg)

def color_print(msg, color = "white", style=0, ret=True):
    cdict = {}
    cdict["white"] = 37
    cdict["red"] = 31
    cdict["green"] = 32
    cdict["yellow"] = 33
    cdict["blue"] = 34
    cdict["magenta"] = 35
    cdict["cyan"] = 36

    if ret:
        msg += "\n"

    if COLOR_OUTPUT:
        sys.stderr.write("\033[%d;%d;40m%s\033[0m" % (style, cdict[color], msg))
    else:
        sys.stderr.write("%s" % (msg))


def split_inp_file(inpfilename):
    """ split .inp file into head, coordinate, and tail """
    state = 0
    inplines_head = []
    inplines_cod = []
    inplines_tail = []
    for line in open(inpfilename):
        line = line.rstrip()
        if state == 0:
            inplines_head.append(line)
            if re.search('data', line, re.IGNORECASE):
                state = 1
        elif state == 1:
            if re.search('[0-9]\.[0-9]', line):
                inplines_cod.append(line)
                state = 2
            else:
                inplines_head.append(line)
        elif state == 2:
            if re.search('[0-9]', line):
                inplines_cod.append(line)
            else:
                inplines_tail.append(line)
                state = 3
        elif state == 3:
            inplines_tail.append(line)


    return inplines_head, inplines_cod, inplines_tail

def find_disp_line(head):
    global DISP_LINE
    idc_line = -1
    for i,line in enumerate(head):
        if re.search("DC.*True", line, re.IGNORECASE):
            DISP_LINE = line
            idc_line = i
    if idc_line == -1:
        return head
    else:
        head.pop(idc_line)
        return head

def kick_line(lines, pattern):
    res = []
    for line in lines:
        if not re.search(pattern, line, re.IGNORECASE):
            res.append(line)
    return res
            
def parse_coord_info(inplines_cod):
    """ get info from coordinate """

    #   O    8.0       0.000000    0.000000    1.877287 ; A charge mul
    #   C    6.0       0.000000    0.000000    0.592015
    #   B    5.0       0.000000    0.785271   -0.674073 ; B charge mul
    #   B    5.0       0.000000   -0.785271   -0.674073

    block_dict = {}
    iatom_to_bname = {}
    charge = 0
    multi = 1
    blockname = "undef"
    for i,line in enumerate(inplines_cod):
        s = line.rstrip().split(';')
        if len(s) == 2:
            coord = s[0]
            infos = s[1].strip().split()
            if infos == []:
                pass
            elif len(infos) == 1:
                blockname = infos[0]
            elif len(infos) == 3:
                blockname, charge, multi = infos
            else:
                error("error in block info")
        elif len(s) == 1:
            coord = s[0]

        if blockname == "undef":
            error("block definition not found")

        if block_dict.has_key(blockname):
            block = block_dict[blockname]
            block._charge_ = charge
            block._multi_ = multi
            block.add_coord(coord)
            block._natom_ += 1
            block._mtb_[i+1] = block._natom_
            block._btm_[block._natom_] = i+1
        else:
            block = BLWblock()
            block._charge_ = charge
            block._multi_ = multi
            block.add_coord(coord)
            block._natom_ += 1
            block._mtb_[i+1] = block._natom_
            block._btm_[block._natom_] = i+1
            block_dict[blockname] = block
        iatom_to_bname[i+1] = blockname

    blocks = []
    for key in block_dict.keys():
        block = block_dict[key]
        block._name_ = key
        blocks.append(block)
        #print block._charge_
        #print block._multi_
        #print block._coord_
        #print block._mtb_
        #print block._btm_

    return blocks, block_dict, iatom_to_bname

def get_basis(atom):
    """ read basis info from files """
    global BASIS_INFO

    if BASIS_INFO.has_key(atom.upper()):
        return BASIS_INFO[atom.upper()]

    res = []
    for line in open("%s/%s.txt" % (BASIS_DIR, atom.upper())):
        res.append(line.rstrip())
    BASIS_INFO[atom.upper()] = res
    return res

def add_basis_info(coord):
    basis = []
    for line in coord:
        basis.append(get_basis(line.split()[0]))
    res = []
    for c,b in zip(coord, basis):
        res.append(c)
        for line in b:
            res.append(line)
    return res
    sys.exit(0)
        

def get_energy0(filename):
    state = 0
    energy = 0.0
    if (BLW_SUFFIX + '0' not in filename) and (not normal_exit(filename)):
        error("error in %s" % filename)
    for line in open(filename):
        if state == 0:
            if re.search("ENERGY COMPONENTS", line):
                state = 1
        elif state == 1:
            if re.search("TOTAL ENERGY", line):
                energy = float(line.split()[-1])
                state = 0
    if energy == 0.0:
        error("SCF not converged: %s" % filename)
    return energy

def get_energy(filename):
    lines = open(filename).readlines()
    # check if disp enabled
    for line in lines:
        if re.search("GRIMME'S EMPIRICAL DISPERSION CORRECTION", line):
            res = line.split()[-1]
            if res == "F":
                disp = False
            elif res == "T":
                disp = True
            else:
                error("Unknown DISPERSION CORRECTION value")

    state = 0
    total_energy = False
    disp_energy = False
    for line in reversed(lines):
        if state == 0:
            if re.search("ddikick.x:", line):
                if not re.search("ddikick.x: exited gracefully.", line):
                    error("job exit with error: %s" % (filename))
                state = 1
        elif state == 1:
            if re.search("TOTAL ENERGY =", line):
                total_energy = float(line.split()[-1])
            elif disp and re.search("EDISP /KCAL,AU:", line):
                disp_energy = float(line.split()[-1])
            if total_energy and disp_energy:
                break
    if total_energy == False:
        error("Total energy not found in %s" % (filename))
    if total_energy == 0.0:
        error("SCF not converged (TOTAL ENERGY = 0): %s" % filename)
    if not disp:
        disp_energy = 0.0

    return total_energy-disp_energy, disp_energy
            

            

def normal_exit(filename):
    for line in reversed(open(filename).readlines()):
        if re.search("ddikick.x: exited gracefully.", line):
            return True
    return False

def get_disp_energy(filename):
    for line in reversed(open(filename).readlines()):
        if re.search("EDISP /KCAL,AU:", line):
            return float(line.split()[-1])
    error("DISP Energy not found in %s" % (filename))

def get_blw0_energy(filename):
    assert(filename.split('.')[1] == "out")
    for line in open(filename):
        if re.search("ITER    1   E\(RBLW\)", line):
            return float(line.strip().split()[-1])


def get_info_from_out(outfilename):
    infos = {}
    for line in open(outfilename):
        if re.search('NUMBER OF CARTESIAN GAUSSIAN BASIS FUNCTIONS', line):
            infos['NB'] = line.strip().split('=')[1].strip()
        elif re.search('NUMBER OF ELECTRONS', line):
            infos['NEL'] = line.strip().split('=')[1].strip()
    return infos

def write_list_to_file(file, alist, keep_blank_line=False):
    for line in alist:
        if (keep_blank_line == True) or (line != ""):
            file.write(line)
            file.write("\n")

def write_blw_head(file):
    file.write(" $blwdat\n")

def write_blw_tail(file):
    file.write(" $end\n")

def add_option(lines, section, option, value):
    """ add option=value to section """
    res = []
    for line in lines:
        if re.search(section, line, re.IGNORECASE):
           if re.search(option, line, re.IGNORECASE):
                line = re.sub("%s *= *\w+" % (option), "%s=%s" % (option, value), line, re.IGNORECASE)
           else:
                line = re.sub("\$end", " %s=%s $end" % (option, value), line, re.IGNORECASE)
        res.append(line)
    return res


def gen_opt_inpfile(head, tail, blocks):
    filename = "all%s.inp" % (OPT_SUFFIX)
    if os.path.isfile(filename):
        if OVERWRITE_INP == False:
            return [filename]
        else:
            os.rename(filename, filename + "~")
    f = open(filename, "w")
    head.insert(0, DISP_LINE)
    head = add_option(head, "contrl", "runtyp", "optimize")
    write_list_to_file(f, head)
    for i,block in enumerate(blocks):
        if READ_BASIS:
            write_list_to_file(f, add_basis_info(block._coord_), keep_blank_line=True)
        else:
            write_list_to_file(f, block._coord_)
    write_list_to_file(f, tail)
    f.close()

    return [filename]

def gen_block_opt_inpfile(head, tail, blocks):
    filenames = []
    head.insert(0, DISP_LINE)
    for i,block in enumerate(blocks):
        filename = "%s%s.inp" % (block._name_, OPT_SUFFIX)
        filenames.append(filename)
        if os.path.isfile(filename):
            if OVERWRITE_INP == False:
                continue
            else:
                os.rename(filename, filename + "~")
        f = open(filename, "w")
        head = add_option(head, "contrl", "runtyp", "optimize")
        head = add_option(head, "contrl", "icharg", "%s" % (block._charge_))
        head = add_option(head, "contrl", "mult", "%s" % (block._multi_))
        write_list_to_file(f, head)
        if READ_BASIS:
            write_list_to_file(f, add_basis_info(block._coord_), keep_blank_line=True)
        else:
            write_list_to_file(f, block._coord_)
        write_list_to_file(f, tail)
        f.close()
    return filenames

def gen_bsse_inpfile(head, tail, coord, block_dict, iatom_to_bname):
    filenames = []
    head.insert(0, " $SCF SOSCF=.TRUE. $END")
    for key in block_dict.keys():
        block = block_dict[key]
        filename = "%s%s.inp" % (block._name_, BSSE_SUFFIX)
        filenames.append(filename)
        if os.path.isfile(filename):
            if OVERWRITE_INP == False:
                continue
            else:
                os.rename(filename, filename + "~")
        f = open(filename, "w")
        head = add_option(head, "contrl", " runtyp", "ENERGY")
        write_list_to_file(f, head)
        if READ_BASIS:
            write_list_to_file(f, add_basis_info(get_bsse_coord(coord, key, iatom_to_bname)), keep_blank_line=True)
        else:
            write_list_to_file(f, get_bsse_coord(coord, key, iatom_to_bname))
        write_list_to_file(f, tail)
        f.close()
    return filenames

def gen_blw0_inpfile(head, tail, blocks, gus):
    filename = "all%s0.inp" % (BLW_SUFFIX) 
    if os.path.isfile(filename):
        if OVERWRITE_INP == False:
            return [filename]
        else:
            os.rename(filename, filename + "~")
    f = open(filename, "w")
    head = add_option(head, "contrl", " runtyp", "ENERGY")
    head.insert(0, "%s" % GRID_LINE)
    head.insert(0, " $BLW NBLOCK=%d ITER=1 $END" % len(blocks))
    write_list_to_file(f, head)
    head.pop(0)
    for i,block in enumerate(blocks):
        if READ_BASIS:
            write_list_to_file(f, add_basis_info(block._coord_), keep_blank_line=True)
        else:
            write_list_to_file(f, block._coord_)
    write_list_to_file(f, tail)
    blwinfos = gen_blw_infos(blocks)

    write_blw_head(f)
    write_list_to_file(f, blwinfos)
    write_list_to_file(f, gus)
    write_blw_tail(f)
    f.close()
    return [filename]


def gen_blw_inpfile(head, tail, blocks):
    filename = "all%s.inp" % (BLW_SUFFIX) 
    if os.path.isfile(filename):
        if OVERWRITE_INP == False:
            return [filename]
        else:
            os.rename(filename, filename + "~")
    f = open(filename, "w")
    head = add_option(head, "contrl", " runtyp", "ENERGY")
    head.insert(0, " $BLW NBLOCK=%d ITER=1000  $END" % len(blocks))
    write_list_to_file(f, head)
    head.pop(0)
    for i,block in enumerate(blocks):
        if READ_BASIS:
            write_list_to_file(f, add_basis_info(block._coord_), keep_blank_line=True)
        else:
            write_list_to_file(f, block._coord_)
    write_list_to_file(f, tail)
    blwinfos = gen_blw_infos(blocks)
    write_blw_head(f)
    write_list_to_file(f, blwinfos)
    #write_list_to_file(f, gus)
    write_blw_tail(f)
    f.close()
    return [filename]

def gen_block_blw_inpfile(head, tail, blocks, method='MO'):
    filenames = []
    if method == "BLW":
        head.insert(0, " $BLW NBLOCK=%d ITER=1000  $END" % 1)
    head.insert(0, DISP_LINE)
    for i,block in enumerate(blocks):
        filename = "%s%s.inp" % (block._name_, BLW_SUFFIX)
        filenames.append(filename)
        if os.path.isfile(filename):
            if OVERWRITE_INP == False:
                continue
            else:
                os.rename(filename, filename + "~")
        f = open(filename, "w")
        head = add_option(head, "contrl", "icharg", "%s" % (block._charge_))
        head = add_option(head, "contrl", "mult", "%s" % (block._multi_))
        head = add_option(head, "contrl", " runtyp", "ENERGY")
        write_list_to_file(f, head)
        if READ_BASIS:
            write_list_to_file(f, add_basis_info(block._coord_), keep_blank_line=True)
        else:
            write_list_to_file(f, block._coord_)
        write_list_to_file(f, tail)
        if method == "BLW":
            write_blw_head(f)
            blwinfos = gen_blw_infos([block])
            write_list_to_file(f, blwinfos)
            write_blw_tail(f)
        f.close()
    head.pop(0)

    return filenames
        

def get_optimized_coord(outfilename):
    p = "EQUILIBRIUM GEOMETRY LOCATED"
    state = 0
    coord = []
    for line in open(outfilename):
        if state == 0:
            if re.search(p, line):
                state = 1
        elif state == 1:
            if re.search('[0-9]\.[0-9]', line):
                coord.append(line)
                state = 2
        elif state == 2:
            if re.search('[0-9]', line):
                coord.append(line)
            else:
                break
    if state != 2:
        error("error when getting optimized geometry in %s" % outfilename)

    return coord

def update_coord(coord, blw_block_dict, iatom_to_bname):

    for i,cor in enumerate(coord):
        block = blw_block_dict[iatom_to_bname[i+1]]
        block._coord_[block._mtb_[i+1] - 1] = cor.rstrip()

def get_bsse_coord(coord, blockname, iatom_to_bname):
    res = []
    for i,cor in enumerate(coord):
        if iatom_to_bname[i+1] != blockname:
            cl = cor.split()
            cl[1] = "-" + cl[1]
            cor = "  ".join(cl)
        res.append(cor.strip())
    return res
             
        

def get_blw_orb(inpfilename):
    """ read blw orbitals from .blw files """
    state = 0
    blw_orb = []
    blw_orb_gauss = []
    for line in open(inpfilename):
        line = line.rstrip()
        if state == 0:
            if re.match(' ---ORBITALS \(LOCAL BFS\)', line):
                state = 1
        elif state == 1:
            if re.search('[0-9]\.[0-9]', line):
                blw_orb.append(line)
            else:
                state = 2
        elif state == 2:
            if re.search(" ---ORBITALS \(FORMATTED\)---", line):
                state = 3
        elif state == 3:
            blw_orb_gauss.append(line)

    return blw_orb, blw_orb_gauss

def gen_blw_infos(blocks):
    lines = []
    for block in blocks:
        line = []
        line.append(block._nel_)
        line.append(block._nb_)
        line.append('1')
        lines.append(' '.join(line))
        line = []
        if len(blocks) == 1:
            line.append(1)
            line.append(int(block._nb_))
        else:
            line.append(block._bas_[0])
            line.append(block._bas_[1])
        lines.append(' '.join([str(i) for i in line]))
    return lines

def get_occupied(jobname):
    for line in open(jobname + ".out"):
        if re.search("ORBITALS ARE OCCUPIED", line):
            return int(line.split()[0])
    error("number of occupied orbitals not found in %s" % jobname + ".out")


def dat_to_blw(jobname):
    state = 0
    datfilename = jobname + ".dat"
    orb = []
    allorb = []
    orbnum = '1'
    oldorbnum = '1'
    nocc = get_occupied(jobname)
    # get orbitals from .dat file
    for line in open(datfilename):
        if state == 0:
            if re.match(" \$VEC", line):
                state = 1
        elif state == 1:
            if re.match(" \$END", line):
                state = 2
                break
            orbnum = line.split()[0]
            if oldorbnum != orbnum:
                oldorbnum = orbnum
                allorb.append(orb)
                orb = []
            if int(orbnum) > nocc:
                break
            orb += re.findall("-?\d\.\d{8}E[+-]\d{2}", line)

    # generate for BLW guess
    blwlines = []
    line = []
    for orb in allorb:
        for i,num in enumerate(orb):
            line.append("%15s" % num)
            if (i+1) % 5 == 0:
                blwlines.append(" ".join(line))
                line = []
        if (i+1) % 5 != 0:
            blwlines.append(" ".join(line))
            line = []
    vfile = open("%s.vec" % jobname, "w")
    write_list_to_file(vfile, blwlines)

    # generate for Gaussian
    gausslines = [" (3E20.10)", ]
    line = []
    for iorb, orb in enumerate(allorb):
        gausslines.append("%4d" % (iorb + 1))
        for i,num in enumerate(orb):
            line.append("%20.10E" % float(num))
            if (i+1) % 3 == 0:
                gausslines.append("".join(line))
                line = []
        if (i+1) % 3 != 0:
            gausslines.append(" ".join(line))
            line = []
    gfile = open("%s.orb" % jobname, "w")
    write_list_to_file(gfile, gausslines)


    return blwlines
            

        

def submit_job(pbs_script, job_name):
    ofilename = get_outnames([job_name])[0]
    if os.path.isfile(ofilename):
        if (normal_exit(ofilename) or (BLW_SUFFIX + '0' in ofilename)) and (OVERWRITE_OUT == False):
            return
        else:
            os.rename(ofilename, ofilename + "~")
    new_pbs_script = job_name + ".pbs.sh"
    state = 0
    newf = open(new_pbs_script, "w")
    for line in open(pbs_script): 
        if state == 0:
            if re.match('#', line):
                if re.search('-N ', line):
                    line = re.sub('-N .*', "-N %s" % (job_name), line)
                if re.search('-pe orte ', line):
                    if re.search(BLW_SUFFIX, job_name):
                        line = re.sub('-pe orte .*', "-pe orte 1", line)
                    else:
                        line = re.sub('-pe orte .*', "-pe orte %d" % (QSUB_NPROC), line)
                if re.search('ppn', line):
                    if re.search(BLW_SUFFIX, job_name):
                        line = re.sub('ppn=.*', "ppn=1", line)
                    else:
                        line = re.sub('ppn=.*', "ppn=%d" % (QSUB_NPROC), line)
            else:
                state = 1
                if re.search("INPUT_FILE", line):
                    line = re.sub("=.*", "=%s" % (job_name), line)
                    state = 2
            newf.write(line)
        elif state == 1:
            if re.search("INPUT_FILE", line):
                line = re.sub("=.*", "=%s" % (job_name), line)
                state = 2
            newf.write(line)
        elif state == 2:
            newf.write(line)
    newf.close()

    print "qsub %s" % (new_pbs_script)
    time.sleep(1.5)
    r = os.system("qsub ./%s" % new_pbs_script)
    if r != 0:
        error("qsub with %s error" % new_pbs_script)

def wait_for_files(filenames, check_inteval=8):

    print "waiting for files: %s" % (" ".join(filenames))

    i = 0
    n = 0
    finished_files = []
    while True:
        i += 1
        old_n = n

        n = 0
        finished_files = []
        for f in filenames:
            if os.path.isfile(f):
                n += 1
                finished_files.append(f)

        if n == len(filenames):
            color_print("all finished: %s\n" % (" ".join(get_jobnames(finished_files))), "green")
            if i > 1:
                print "total time: %d seconds\n" % (check_inteval * (i-1))
            return
        else:
            if (old_n != n) or (i == 1):
                color_print("%d/%d " % (n,len(filenames)), "yellow", ret=False)
                sys.stderr.write("finished: %s\n" % (" ".join(get_jobnames(finished_files))))
        time.sleep(check_inteval)
            
def get_jobnames(name):
    return [n.split('.')[0] for n in name]

def get_outnames(name):
    return [n.split('.')[0] + ".out" for n in name]

def get_inpnames(name):
    return [n.split('.')[0] + ".inp" for n in name]

def get_blwnames(name):
    return [n.split('.')[0] + ".blw" for n in name]

def get_names(name, suffix):
    return [n.split('.')[0] + suffix for n in name]

def print_usage():
    print
    print "Usage: %s [-o -i -n <nproc> -p -c] <.inp file> <gamess script> <blw script>" % (sys.argv[0])
    print
    print
    print "\t-o:  overwrite existing .out files"
    print "\t-i:  overwrite existing .inp files"
    print "\t-c:  colorful output"
    print "\t-p:  generate .inp files for geometry optimizations only"
    print "\t-n <nproc>:  set number of CPU in qsub script for MO calculations to nproc (default 8)"
    print "\t-r <basis dir>:  read user define basis functions from basis dir"
    print
    sys.exit(0)

def main():
    #if len(sys.argv) != 3:
    #    print "Usage: %s <.inp file> <script for qsub>" % (sys.argv[0])
    #    sys.exit(0)
    global OVERWRITE_OUT
    global OVERWRITE_INP
    global QSUB_NPROC
    global OPT_ONLY
    global COLOR_OUTPUT
    global READ_BASIS
    global BASIS_DIR

    try:
        opts, args = getopt.getopt(sys.argv[1:], "ion:pcr:", ["oi", "oo", "nproc", "optonly", "color", "readbasis"])
    except getopt.GetoptError:  
        error("unknown parameters")

    for o,a in opts:
        if o in ("-i", "--oi"):
            OVERWRITE_INP = True
        elif o in ("-o", "--oo"):
            OVERWRITE_OUT = True
        elif o in ("-n", "--nproc"):
            QSUB_NPROC = int(a)
        elif o in ("-p", "--optonly"):
            OPT_ONLY = True
        elif o in ("-c", "--color"):
            COLOR_OUTPUT = True
        elif o in ("-r", "--readbasis"):
            READ_BASIS = True
            BASIS_DIR = a
            

    if len(args) != 3:
        print_usage()

    resfile = open("finalresult.txt", "w")

    inputOptFileName = args[0]
    disp_script = args[1]
    blw_script = args[2]
    head, coord, tail =  split_inp_file(inputOptFileName) 
    head = find_disp_line(head)
    if READ_BASIS:
        head = kick_line(head, "\$basis")
    blw_blocks, blw_block_dict, iatom_to_bname = parse_coord_info(coord)
    

    # geometry opt
    print "generating opt inp files"
    opt_filenames = gen_opt_inpfile(list(head), tail, blw_blocks)
    inputOptFileName = opt_filenames[0]
    block_opt_filenames = []
    block_opt_filenames += gen_block_opt_inpfile(list(head), tail, blw_blocks)
    
    if OPT_ONLY:
        return

    # submit opt jobs
    print "submitting opt inp files"
    for f in get_jobnames(opt_filenames + block_opt_filenames):
        submit_job(disp_script, f)
        
    # wait for opt jobs to complete
    wait_for_files(get_outnames(opt_filenames + block_opt_filenames))

    # get result energy
    opt_energy, opt_disp_energy = get_energy(get_outnames(opt_filenames)[0])
    block_opt_energy = []
    block_opt_disp_energy = []
    for ofile in get_outnames(block_opt_filenames):
        t, d = get_energy(ofile)
        block_opt_energy.append(t)
        block_opt_disp_energy.append(d)

    # update basis and electron infos from opt out files to blocks
    nb_h = 0
    nb_t = 0
    for key in blw_block_dict.keys():
        block = blw_block_dict[key]
        infos = get_info_from_out("%s%s.out" % (key, OPT_SUFFIX))
        block._nb_ = infos["NB"]
        block._nel_ = infos["NEL"]
        nb_h += 1
        nb_t = nb_h + int(block._nb_) - 1
        block._bas_ = [nb_h, nb_t]
        nb_h = nb_t

    # update coordinate with optimized ones
    outputOptFilename = inputOptFileName.split('.')[0] + ".out"
    opted_coord = get_optimized_coord(outputOptFilename)
    update_coord(opted_coord, blw_block_dict, iatom_to_bname)

    print "generating bsse and blw inp files"
    bsse_filenames = gen_bsse_inpfile(list(head), tail, opted_coord, blw_block_dict, iatom_to_bname)

    # construct blw input files
    block_blw_filenames = gen_block_blw_inpfile(list(head), tail, blw_blocks, method = BLOCK_BLW_METHOD)
    blw_filenames = gen_blw_inpfile(list(head), tail, blw_blocks)
    # submit block blw jobs
    print "submitting blw and bsse jobs"
    for f in get_jobnames(bsse_filenames + block_blw_filenames):
        submit_job(disp_script, f)
    for f in get_jobnames(blw_filenames):
        submit_job(blw_script, f)

    wait_for_files(get_outnames(block_blw_filenames))

    # get result energy
    block_blw_energy = []
    block_blw_disp_energy = []
    for ofile in get_outnames(block_blw_filenames):
        t,d = get_energy(ofile)
        block_blw_energy.append(t)
        block_blw_disp_energy.append(d)
    

    # submit blw0 job
    print "submitting blw job"
    # construct guess for blw job
    gus = []
    if BLOCK_BLW_METHOD == "BLW":
        for block in blw_blocks:
            gus += get_blw_orb("%s%s.blw" % (block._name_, BLW_SUFFIX))[0]
    else:
        for ofile in get_jobnames(block_blw_filenames):
            gus += dat_to_blw(ofile)

    blw0_filenames = gen_blw0_inpfile(list(head), tail, blw_blocks, gus)
    for f in get_jobnames(blw0_filenames):
        submit_job(blw_script, f)

    # wait for blw0 jobs to complete
    wait_for_files(get_outnames(blw_filenames + blw0_filenames + bsse_filenames))

    blw_energy, blw_disp_energy = get_energy(get_outnames(blw_filenames)[0])
    blw0_energy = get_blw0_energy(get_outnames(blw0_filenames)[0])
    # write vec and orb for blw and blw0
    vec, orb = get_blw_orb(get_blwnames(blw_filenames)[0])
    f = open(get_names(blw_filenames, ".vec")[0], "w")
    write_list_to_file(f, vec)
    f = open(get_names(blw_filenames, ".orb")[0], "w")
    write_list_to_file(f, orb)

    vec, orb = get_blw_orb(get_blwnames(blw0_filenames)[0])
    f = open(get_names(blw0_filenames, ".vec")[0], "w")
    write_list_to_file(f, vec)
    f = open(get_names(blw0_filenames, ".orb")[0], "w")
    write_list_to_file(f, orb)

    # bsse energy
    bsse_energy = []
    bsse_disp_energy = []
    for ofile in get_outnames(bsse_filenames):
        t,d  = get_energy(ofile)
        bsse_energy.append(t)
        bsse_disp_energy.append(d)


    # calculate bsse

    # calculate final results
    # E-def: A + B - A-OPT - B-OPT
    # E-HL:  BLW(inter=1) - A - B
    # E-pol: BLW - BLW(inter=1)
    # BSSE:  A + B - A-BSSE - B-BSSE
    # E-CT:  OPT - BLW + BSSE
    # print opt_energy
    # print block_opt_energy
    # print blw_energy
    # print block_blw_energy

    color_print("==============================================", "white", 0)
    titlecolor = "cyan"
    color_print("BLOCK OPT RESULT", titlecolor, 0)
    for i,key in enumerate(blw_block_dict.keys()):
        print "%s\t%s" % (key, block_opt_energy[i])
    print

    color_print("OPT RESULT", titlecolor, 0)
    print "opt energy: %s" % opt_energy
    print

    color_print("BLOCK BSSE RESULT", titlecolor, 0)
    for i,key in enumerate(blw_block_dict.keys()):
        print "%s\t%s" % (key, bsse_energy[i])
    print

    color_print("BLOCK BLW RESULT", titlecolor, 0)
    for i,key in enumerate(blw_block_dict.keys()):
        print "%s\t%s" % (key, block_blw_energy[i])
    print

    color_print("BLW RESULT", titlecolor, 0)
    print "blw energy: %s" % blw_energy
    print "blw0 energy: %s" % blw0_energy
    print
    color_print("==============================================", "white", 0)

        

    E_def = sum(block_blw_energy) - sum(block_opt_energy) + sum(block_blw_disp_energy) - sum(block_opt_disp_energy)
    E_HL = blw0_energy - sum(block_blw_energy)
    E_pol = blw_energy - blw0_energy 
    E_BSSE = sum(block_blw_energy) - sum(bsse_energy)
    E_CT = opt_energy + E_BSSE - blw_energy
    E_disp = opt_disp_energy - sum(block_blw_disp_energy)

    color_print("FINAL RESULT", titlecolor, 1)
    color = "white"
    color_print("E_def\t%12.7f" % (E_def), color, 1)
    color_print("E_HL\t%12.7f" % (E_HL), color, 1)
    color_print("E_pol\t%12.7f" % (E_pol), color, 1)
    color_print("E_BSSE\t%12.7f" % (E_BSSE), color, 1)
    color_print("E_CT\t%12.7f" % (E_CT), color, 1)
    color_print("E_disp\t%12.7f" % (E_disp), color, 1)

    ratio = 627.51

    write_log(" \t%12s\t%12s" % ("a.u.", "kcal/mol"), resfile)
    write_log("E_def\t%12.7f\t%12.7f" % (E_def, E_def*ratio), resfile)
    write_log("E_HL\t%12.7f\t%12.7f" % (E_HL, E_HL*ratio), resfile)
    write_log("E_pol\t%12.7f\t%12.7f" % (E_pol, E_pol*ratio), resfile)
    write_log("E_BSSE\t%12.7f\t%12.7f" % (E_BSSE, E_BSSE*ratio), resfile)
    write_log("E_CT\t%12.7f\t%12.7f" % (E_CT, E_CT*ratio), resfile)
    write_log("E_disp\t%12.7f\t%12.7f" % (E_disp, E_disp*ratio), resfile)

    write_log("\n%14s%14s%14s%14s%14s%14s%14s" % (" ", "E_def", "E_HL", "E_pol","E_BSSE","E_CT","E_disp"), resfile)
    write_log("%14s%14.7f%14.7f%14.7f%14.7f%14.7f%14.7f" % ("#blwdata_a.u._#",E_def,E_HL,E_pol,E_BSSE,E_CT,E_disp), resfile)
    write_log("%14s%14.7f%14.7f%14.7f%14.7f%14.7f%14.7f" % ("#blwdata_kcal_#",E_def*ratio,E_HL*ratio,E_pol*ratio,E_BSSE*ratio,E_CT*ratio,E_disp*ratio), resfile)

    print "=============================================="
    print

BASIS_INFO = {}
BASIS_DIR = ""
READ_BASIS = False

OPT_SUFFIX = "-opt-ab"
BLW_SUFFIX = "-blw-ab"
BSSE_SUFFIX = "-bsse-ab"
OVERWRITE_INP = False
OVERWRITE_OUT = False
OPT_ONLY = False
QSUB_NPROC = 8
COLOR_OUTPUT = False
DISP_LINE = ""
GRID_LINE = " $DFT NRAD0=96 NTHE0=12 NPHI0=24 NLEB0=302 $END"
BLOCK_BLW_METHOD = "MO"

main()
